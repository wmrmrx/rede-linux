#!/bin/bash
set -e

cp configs/nsswitch.conf /etc/nsswitch.conf

cp configs/resolv.conf /etc/resolv.conf

cp configs/common-session /etc/pam.d/common-session

cp configs/pam_mount.conf.xml /etc/security/pam_mount.conf.xml

cp configs/krb5.conf /etc/krb5.conf

cp configs/k5login /root/.k5login

cp add_keytabs.sh /root/add_keytabs.sh

cp configs/ldap.conf /etc/ldap/ldap.conf

cp configs/sssd.conf /etc/sssd/sssd.conf
chmod 0600 /etc/sssd/sssd.conf

cp configs/resolv.conf /etc/resolv.conf

cp configs/redelinuxCA.crt /usr/local/share/ca-certificates/redelinuxCA.crt
chmod 0700 /usr/local/share/ca-certificates/redelinuxCA.crt
