#!/bin/bash
set -e

echo ""

echo "**Não** rode este programa depois da instalação,"
echo "ele invoca o 'kadmin' com sua senha, para a sua "
echo "conveniência. Isso a exporia via process list.  "
echo "Se apenas você estiver na máquina, tudo bem.    "

echo ""

echo "Lista de Usuários:"

who

echo ""

echo "Entre com seu Usuário, admin:"
echo -n "> "
read ADMIN

ADMIN=$ADMIN/admin

echo "Entre com a senha para $ADMIN@LINUX.IME.USP.BR:"
echo -n "> "
read -s PASSW

HOSTNAME=$(</etc/hostname)

rm -f /etc/krb5.keytab

kadmin -p $ADMIN -w $PASSW -q "addprinc -randkey -policy host   host/$HOSTNAME"
kadmin -p $ADMIN -w $PASSW -q "addprinc -randkey -policy service nfs/$HOSTNAME"
kadmin -p $ADMIN -w $PASSW -q "addprinc -randkey -policy service ipp/$HOSTNAME"

kadmin -p $ADMIN -w $PASSW -q "ktadd host/$HOSTNAME"
kadmin -p $ADMIN -w $PASSW -q "ktadd  nfs/$HOSTNAME"
kadmin -p $ADMIN -w $PASSW -q "ktadd  ipp/$HOSTNAME"

chmod 0700 /etc/krb5.keytab

mv /etc/krb5.keytab /identidade/etc/krb5.keytab
ln -s /identidade/etc/krb5.keytab /etc/krb5.keytab
