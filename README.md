# Estrutura dos subvolumes
```
/dev/sda2
├── commons
│   ├── @home
│   ├── @root-home
│   ├── @tmp
│   ├── @swap
│   └── @var@log
└── @rootfs
```

# Boot

Mudar o /etc/fstab para
```
/dev/sda2 /               btrfs   defaults 0       0
/dev/sda1 /boot/efi       vfat    umask=0077      0       1
```

Rodar bootctl install

Instalar dracut e mudar /etc/kernel/postinst.d/dracut para

`dracut -q --force --uefi /boot/efi/EFI/Linux/default.efi >&2`

(Rodar o comando acima uma vez ou reinstalar o kernel)
