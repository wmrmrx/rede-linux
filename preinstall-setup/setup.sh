#!/bin/bash
set -e

echo "Partição de btrfs da root a ser copiada (do USB)?"
read SRC
echo "Partição de btrfs do destino?"
read DEST
echo Fazer setup de "$SRC" para "$DEST"? '(y/N)'
read PROMPT
if [ ! "$PROMPT" == "y" ]; then exit; fi

cd /mnt
mkdir src
mkdir dest
mount $SRC src
mount $DEST dest

# Copiar a root do USB para o disco
echo 'Executando btrfs send src/@origin | btrfs receive dest...'
btrfs send 'src/@origin' | btrfs receive 'dest'
btrfs subvolume snapshot 'dest/@origin' 'dest/@rootfs'
echo 'Concluido!'

# Adicionar arquivos locais
mkdir dest/commons
btrfs subvolume create 'dest/commons/@identidade'
mkdir dest/commons/@identidade/etc
echo "Hostname (nome da máquina)?"
read HOSTNAME
echo $HOSTNAME > dest/commons/@identidade/etc/hostname

btrfs subvolume create 'dest/commons/@home'

umount src
rmdir src
umount dest
rmdir dest

cd /
mount "$DEST" /mnt -o subvol=@rootfs
for dir in /proc /sys /dev /run; do mount --bind $dir /mnt$dir; done
echo Instalando GRUB...

PATH=/sbin:$PATH
chroot /mnt /bin/bash -c 'mount /boot/efi && grub-install'

echo Resta adicionar o keytab!
chroot /mnt /bin/bash -c 'mount /identidade && /root/add_keytabs.sh'

umount -R /mnt
echo "Setup concluido com sucesso"
