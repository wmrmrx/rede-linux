#!/bin/bash
set -e

DISK=$1

echo Formatar "$DISK"? '(y/N)'
read PROMPT
if [ ! "$PROMPT" = "y" ]; then
	exit
fi

# Criar a tabela de partições
sgdisk -og "$DISK"
sgdisk -n 1:+1M:+512M -c 1:"EFI System Partition" --typecode=1:ef00 "$DISK" 
sgdisk -n 2:0:-1G -c 2:"Linux filesystem" --typecode=2:8300 "$DISK"
sgdisk -n 3:0:+0 -c 3:"Linux swap partition" --typecode=3:8200 "$DISK"

# Para tratar diferença entre numeração de partições
# /dev/sdX: /dev/sdX1, /dev/sdX2... VS /dev/nvmenX: /dev/nvmenXp1, /dev/nvmeXp2...
if [[ "$DISK" == *"nvme"* ]]; then
	DISK="$DISK"p
fi

# Apenas formatação, especificar os UUIDs para usar uma configuração universal pro GRUB
mkfs.fat -F 32 "$DISK"1 -n "EFIZIN"
mkfs.btrfs -f "$DISK"2 --label "ROOTZIN"
mkswap "$DISK"3 --label "SWAPZIN"
