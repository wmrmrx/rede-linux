#!/bin/bash
set -e

DISK=$1

echo Formatar "$DISK"? '(y/N)'
read PROMPT
if [ ! "$PROMPT" = "y" ]; then
	exit
fi

# Criar a tabela de partições
sgdisk -og "$DISK"
sgdisk -n 1:0:+1M -c 1:"BIOS boot partition" --typecode=1:ef02 "$DISK" 
sgdisk -n 2:0:+512M -c 2:"EFI System Partition" --typecode=2:ef00 "$DISK" 
sgdisk -n 3:0:+1G -c 3:"Linux swap partition" --typecode=3:8200 "$DISK"
sgdisk -n 4:0:-1M -c 4:"Linux filesystem" --typecode=4:8300 "$DISK"

# Para tratar diferença entre numeração de partições
# /dev/sdX: /dev/sdX1, /dev/sdX2... VS /dev/nvmenX: /dev/nvmenXp1, /dev/nvmeXp2...
if [[ "$DISK" == *"nvme"* ]]; then
	DISK="$DISK"p
fi

# Apenas formatação, especificar os UUIDs para usar uma configuração universal pro GRUB
mkfs.fat -F 32 "$DISK"2 -n "EFIZIN" -i abababab
mkswap "$DISK"3 --label "SWAPZIN" -U cdcdcdcd-cdcd-cdcd-cdcd-cdcdcdcdcdcd
# Para evitar erros com UUID repitidos
mkfs.btrfs -f "$DISK"4 &> /dev/null
mkfs.btrfs -f "$DISK"4 --label "ROOTZIN" -U efefefef-efef-efef-efef-efefefefefef
